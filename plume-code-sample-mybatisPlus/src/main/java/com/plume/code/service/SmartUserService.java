package com.plume.code.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.plume.code.mapper.entity.SmartUserENT;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-05 11:13:50
 **/
public interface SmartUserService extends IService<SmartUserENT> {

}
