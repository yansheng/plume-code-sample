package com.plume.code.service.impl;

import com.plume.code.mapper.SmartUserMapper;
import com.plume.code.mapper.entity.SmartUserENT;
import com.plume.code.service.SmartUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-05 11:13:50
 **/
@Service
public class SmartUserServiceImpl extends ServiceImpl<SmartUserMapper, SmartUserENT> implements SmartUserService {

}
