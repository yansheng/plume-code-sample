package com.plume.code.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.plume.code.mapper.entity.SmartUserENT;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-05 11:13:50
 **/
@Mapper
public interface SmartUserMapper extends BaseMapper<SmartUserENT> {

}
