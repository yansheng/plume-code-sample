package com.plume.code.admin.controller;

import com.github.pagehelper.PageInfo;
import com.plume.code.admin.controller.query.SmartUserQuery;
import com.plume.code.mapper.entity.SmartUserENT;
import com.plume.code.service.SmartUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-05 11:13:50
 **/
@RestController
@RequestMapping("/admin/smartUser/")
public class SmartUserController {

    @Autowired
    private SmartUserService smartUserService;

    @PostMapping("page")
    public PageInfo<SmartUserENT> page(@RequestBody SmartUserQuery query) {
      return smartUserService.page(query);
    }

    @PostMapping(value = "save")
    public void save(@RequestBody SmartUserENT ent) {
        smartUserService.save(ent);
    }

    @PostMapping(value = "update")
    public void update(@RequestBody SmartUserENT ent) {
        smartUserService.updateById(ent);
    }

    @PostMapping(value = "delete")
    public void delete(@RequestParam("id") Long id) {
        smartUserService.removeById(id);
    }
}
