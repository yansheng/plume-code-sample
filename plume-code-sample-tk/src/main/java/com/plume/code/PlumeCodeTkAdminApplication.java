package com.plume.code;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
// 这里的MapperScan是tk的；而不是mybatis的
@MapperScan("com.plume.code.mapper")
public class PlumeCodeTkAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlumeCodeTkAdminApplication.class, args);
    }
}
