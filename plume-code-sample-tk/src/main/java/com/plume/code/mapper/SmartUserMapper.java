package com.plume.code.mapper;

import com.plume.code.mapper.entity.SmartUserENT;
import tk.mybatis.mapper.common.Mapper;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-05 11:13:50
 **/
public interface SmartUserMapper extends Mapper<SmartUserENT>  {

}
