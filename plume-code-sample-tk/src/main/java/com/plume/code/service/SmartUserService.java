package com.plume.code.service;

import com.plume.code.mapper.entity.SmartUserENT;
import com.plume.code.admin.controller.query.SmartUserQuery;
import com.github.pagehelper.PageInfo;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 14:42:56
 **/
public interface SmartUserService  {
    PageInfo<SmartUserENT> page(SmartUserQuery query);

    void save(SmartUserENT smartUserENT);

    void updateById(SmartUserENT smartUserENT);

    void removeById(Long id);
}
