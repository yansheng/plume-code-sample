package com.plume.code.service.dto;


import java.io.Serializable;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 14:42:56
 **/
public class SmartUserDTO implements Serializable {

    /**
     * comment:ID
     */
    private Long id;

    /**
     * comment:姓名
     */
    private String name;

    /**
     * comment:密码
     */
    private String password;

    /**
     * comment:version
     */
    private Long version;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

}

