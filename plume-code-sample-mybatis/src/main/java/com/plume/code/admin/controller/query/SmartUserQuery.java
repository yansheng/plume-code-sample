package com.plume.code.admin.controller.query;


import java.io.Serializable;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 11:46:37
 **/
public class SmartUserQuery implements Serializable {
    protected Integer pageIndex = 1;
    protected Integer pageSize = 10;

    /**
     * comment:ID
     */
    private Long id;

    /**
     * comment:姓名
     */
    private String name;

    /**
     * comment:密码
     */
    private String password;

    /**
     * comment:version
     */
    private Long version;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}

