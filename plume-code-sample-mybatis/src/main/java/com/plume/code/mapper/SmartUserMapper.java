package com.plume.code.mapper;

import com.plume.code.admin.controller.query.SmartUserQuery;
import  com.plume.code.mapper.entity.SmartUserENT;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 11:51:12
 **/
public interface SmartUserMapper {
    List<SmartUserENT> page(@Param("q") SmartUserQuery query);

    int deleteByPrimaryKey(Long id);

    int insert(SmartUserENT smartUserENT);

    int insertSelective(SmartUserENT smartUserENT);

    SmartUserENT selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SmartUserENT smartUserENT);

    int updateByPrimaryKey(SmartUserENT smartUserENT);
}
