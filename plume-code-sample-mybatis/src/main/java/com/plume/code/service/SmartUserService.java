package com.plume.code.service;

import com.github.pagehelper.PageInfo;
import com.plume.code.mapper.entity.SmartUserENT;
import com.plume.code.admin.controller.query.SmartUserQuery;
import com.github.pagehelper.Page;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 11:46:37
 **/
public interface SmartUserService  {
    PageInfo<SmartUserENT> page(SmartUserQuery query);

    void save(SmartUserENT smartUserENT);

    void updateById(SmartUserENT smartUserENT);

    void removeById(Long id);
}
