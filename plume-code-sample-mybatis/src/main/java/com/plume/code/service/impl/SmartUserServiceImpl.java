package com.plume.code.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.plume.code.mapper.entity.SmartUserENT;
import com.plume.code.admin.controller.query.SmartUserQuery;
import com.plume.code.mapper.SmartUserMapper;
import com.plume.code.service.SmartUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 11:46:37
 **/
@Service
public class SmartUserServiceImpl implements SmartUserService {

    @Autowired
    private SmartUserMapper smartUserMapper;

    @Override
    public PageInfo<SmartUserENT> page(SmartUserQuery query) {
        PageInfo<SmartUserENT> page = PageHelper.startPage(query.getPageIndex(), query.getPageSize())
                                .doSelectPageInfo(()-> smartUserMapper.page(query));
        return page;
    }

    @Override
    public void save(SmartUserENT smartUserENT) {
        smartUserMapper.insert(smartUserENT);
    }

    @Override
    public void updateById(SmartUserENT smartUserENT) {
        smartUserMapper.updateByPrimaryKeySelective(smartUserENT);
    }

    @Override
    public void removeById(Long id) {
        smartUserMapper.deleteByPrimaryKey(id);
    }
}
