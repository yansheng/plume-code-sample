package com.plume.code;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.plume.code.mapper")
public class PlumeCodeMybatisAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlumeCodeMybatisAdminApplication.class, args);
    }
}
