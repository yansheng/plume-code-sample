package com.plume.code.repository;

import com.plume.code.repository.entity.SmartUserENT;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 10:47:42
 **/
public interface SmartUserRepository extends JpaRepository<SmartUserENT, Long> {

}
