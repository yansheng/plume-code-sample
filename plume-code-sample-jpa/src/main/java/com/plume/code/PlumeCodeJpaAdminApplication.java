package com.plume.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlumeCodeJpaAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlumeCodeJpaAdminApplication.class, args);
    }
}
