package com.plume.code.admin.controller;

import com.plume.code.admin.controller.query.SmartUserQuery;
import com.plume.code.repository.entity.SmartUserENT;
import com.plume.code.repository.SmartUserRepository;
import com.plume.code.service.SmartUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 10:47:42
 **/
@RestController
@RequestMapping("/admin/smartUser/")
public class SmartUserController {

    @Autowired
    private SmartUserService smartUserService;
    @Autowired
    private SmartUserRepository smartUserRepository;

    @PostMapping("page")
    public Page<SmartUserENT> page(@RequestBody SmartUserQuery query) {
      return smartUserService.page(query);
    }

    @PostMapping(value = "save")
    public void save(@RequestBody SmartUserENT ent) {
        smartUserRepository.save(ent);
    }

    @PostMapping(value = "update")
    public void update(@RequestBody SmartUserENT ent) {
        smartUserRepository.save(ent);
    }

    @PostMapping(value = "delete")
    public void delete(@RequestBody SmartUserENT ent) {
        smartUserRepository.delete(ent);
    }
}
