package com.plume.code.service;

import com.plume.code.admin.controller.query.SmartUserQuery;
import com.plume.code.repository.entity.SmartUserENT;
import org.springframework.data.domain.Page;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 10:47:42
 **/
public interface SmartUserService  {
    Page<SmartUserENT> page(SmartUserQuery query);
}
