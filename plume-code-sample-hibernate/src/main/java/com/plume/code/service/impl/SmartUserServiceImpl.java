package com.plume.code.service.impl;

import com.plume.code.repository.entity.SmartUserENT;
import com.plume.code.admin.controller.query.SmartUserQuery;
import com.plume.code.repository.SmartUserRepository;
import com.plume.code.service.SmartUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
/**
 * @description: ${comment}
 * @author: plume-code
 * @date: 2021-07-15 10:47:42
 **/
@Service
public class SmartUserServiceImpl implements SmartUserService {

    @Autowired
    private SmartUserRepository smartUserRepository;

    @Override
    public Page<SmartUserENT> page(SmartUserQuery query) {
        //jpa zero-based page index
        Pageable pageable = PageRequest.of(query.getPageIndex() - 1, query.getPageSize());
        return smartUserRepository.findAll(pageable);
    }
}
