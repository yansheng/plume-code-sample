package com.plume.code.repository.entity;

import javax.persistence.*;

/**
 * @description:
 * @author: plume-code
 * @date: 2021-07-15 10:47:42
 **/
@Entity
@Table(name = "SMART_USER")
public class SmartUserENT {

    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "VERSION")
    private Long version;

    public Long getId() {
    return id;
    }

    public void setId(Long id) {
    this.id = id;
    }

    public String getName() {
    return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    public String getPassword() {
    return password;
    }

    public void setPassword(String password) {
    this.password = password;
    }

    public Long getVersion() {
    return version;
    }

    public void setVersion(Long version) {
    this.version = version;
    }

}
