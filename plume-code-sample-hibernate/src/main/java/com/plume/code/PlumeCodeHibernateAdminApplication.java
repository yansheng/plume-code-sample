package com.plume.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlumeCodeHibernateAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlumeCodeHibernateAdminApplication.class, args);
    }
}
